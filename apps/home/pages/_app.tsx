import React from 'react';
import { AppProps } from 'next/app';
import { ThemeProvider } from '@waweb/theme';
import Layout from '@waweb/layout';

import './styles.css';

export default function MyApp(props: AppProps) {
  const { Component, pageProps } = props;
  return (
    <ThemeProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ThemeProvider>
  );
}
