export type { ThemeProviderProps } from './lib/theme-provider';
export { ThemeProvider } from './lib/theme-provider';
export { default } from './lib/theme';
