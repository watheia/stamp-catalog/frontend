import { blueGrey, red, teal } from '@mui/material/colors';
export default {
  primary: blueGrey,
  secondary: teal,
  error: {
    main: red.A400,
  },
};
