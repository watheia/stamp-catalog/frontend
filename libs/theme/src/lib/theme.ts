import { createTheme } from '@mui/material/styles';
import palette from './palette';

export * from './theme-provider';
export type { ThemeProviderProps } from './theme-provider';

// Create a theme instance.
export const theme = createTheme({
  palette,
});

export default theme;
