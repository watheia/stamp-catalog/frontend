import React from 'react';
import MuiAppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Link } from '@waweb/atoms';

/* eslint-disable-next-line */
export interface AppBarProps {}

export function AppBar(props: AppBarProps) {
  return (
    <MuiAppBar
      position="static"
      color="default"
      elevation={0}
      sx={{ borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}
    >
      <Toolbar sx={{ flexWrap: 'wrap' }}>
        <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
          <Link
            variant="inherit"
            color="inherit"
            href="http://localhost:4200/"
            underline="hover"
          >
            waweb
          </Link>
        </Typography>
        <nav>
          <Link
            variant="button"
            color="text.primary"
            activeClassName="text.secondary"
            href="/"
            sx={{ my: 1, mx: 1.5 }}
          >
            Home
          </Link>
          <Link
            variant="button"
            color="text.primary"
            href="http://localhost:4200/about"
            sx={{ my: 1, mx: 1.5 }}
          >
            About
          </Link>
          <Link
            variant="button"
            color="text.primary"
            href="http://localhost:4200/blog"
            sx={{ my: 1, mx: 1.5 }}
          >
            Blog
          </Link>
        </nav>
        <Button href="#" variant="outlined" sx={{ my: 1, mx: 1.5 }}>
          Login
        </Button>
      </Toolbar>
    </MuiAppBar>
  );
}

export default AppBar;
