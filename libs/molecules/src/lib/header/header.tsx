import styled from '@emotion/styled';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import React from 'react';

/* eslint-disable-next-line */
export interface HeaderProps {
  title?: string;
  subTitle?: string;
}

const StyledHeader = styled.div`
  color: pink;
`;

export function Header({ title, subTitle }: HeaderProps) {
  if (!title && !subTitle) return null;

  return (
    <Container disableGutters maxWidth="sm" component="main" sx={{ pt: 8, pb: 6 }}>
      {title && (
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="text.primary"
          gutterBottom
        >
          {title}
        </Typography>
      )}
      {subTitle && (
        <Typography variant="h5" align="center" color="text.secondary" component="p">
          {subTitle}
        </Typography>
      )}
    </Container>
  );
}

export default Header;
